Data Analytics with HPC: Hadoop Practical
========================================

This practical can be done using either R or python:

  - [Instructions for R version](readme_R.md)
  - [Instructions for Python version](readme_python.md)
